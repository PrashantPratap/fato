﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FATOWeb.Controllers.FATO
{
    // [SessionState(System.Web.SessionState.SessionStateBehavior.ReadOnly)]

    public class FATOMainController : Controller
    {
        // GET: FATOMain
        public ActionResult Index()
        {
            Models.FATO.FATOInputModel m = new Models.FATO.FATOInputModel();

            var geos = new List<SelectListItem>();

            geos.Add(new SelectListItem() { Text = "Dallas", Value = "Dallas" });
            geos.Add(new SelectListItem() { Text = "New York", Value = "New York" });

            m.GeoLocations = geos;

            var wcs = new List<SelectListItem>();
            wcs.Add(new SelectListItem() { Text = "Irving", Value = "Irving" });
            wcs.Add(new SelectListItem() { Text = "Frisco", Value = "Frisco" });
            m.WorkCenters = wcs;

            m.ScanType = 2;

            return View(m);
        }

        public JsonResult GetWorkCenters(string id)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            var wcs = new List<SelectListItem>();

            switch (id)
            {
                case "Dallas":
                    {
                        wcs.Add(new SelectListItem() { Text = "Irving", Value = "Irving" });
                        wcs.Add(new SelectListItem() { Text = "Frisco", Value = "Frisco" });
                        break;
                    }
                case "New York":
                    {
                        wcs.Add(new SelectListItem() { Text = "Time Square", Value = "Time Square" });
                        wcs.Add(new SelectListItem() { Text = "New Jersey", Value = "New Jersey" });
                        break;
                    }
            }

            result.Data = wcs.ToList();
            
            return result;
        }

        public ActionResult FATODetails(string id)
        {
            Models.FATO.FATODetailsModel m = new Models.FATO.FATODetailsModel();
            if (id==null)
            {
                return PartialView(m);
            }
            
            switch (id.First())
            {
                case '1':
                    {
                        m.BCN = "oneBCN";
                        m.ClientName = "oneClient";
                        m.CompanyName = "oneCompany";
                        m.PartNumber = "onePart";
                        
                        break;
                    }
                case '2':
                    {
                        m.BCN = "twoBCN";
                        m.ClientName = "twoClient";
                        m.CompanyName = "twoCompany";
                        m.PartNumber = "twoPart";

                        break;
                    }
                default:
                    {
                        m.BCN = $"{id}BCN";
                        m.ClientName = $"{id}Client";
                        m.CompanyName = $"{id}Company";
                        m.PartNumber = $"{id}Part";

                        break;
                    }
            }
            return PartialView(m);
        }

        public ActionResult FATOCode(string id)
        {
            Models.FATO.FATOCodeModel m = new Models.FATO.FATOCodeModel();
            if(string.IsNullOrEmpty(id))
            {
                return PartialView(m);
            }

            var acs = new List<SelectListItem>();
            var dcs = new List<SelectListItem>();


            switch (id.First())
            {
                case '1':
                    {
                        m.ActionCode = "oneActionCode";
                        m.DefectCode = "oneDefectCode";

                        acs.Add(new SelectListItem() { Value = "oneActionCode", Text = "oneActionCode" });
                        acs.Add(new SelectListItem() { Value = "oneActionCode2", Text = "oneActionCode2" });
                        acs.Add(new SelectListItem() { Value = "oneActionCode3", Text = "oneActionCode3" });

                        dcs.Add(new SelectListItem() { Value = "oneDefectCode", Text = "oneDefectCode" });
                        dcs.Add(new SelectListItem() { Value = "oneDefectCode2", Text = "oneDefectCode2" });
                        dcs.Add(new SelectListItem() { Value = "oneDefectCode3", Text = "oneDefectCode3" });

                        break;
                    }
                case '2':
                    {
                        m.ActionCode = "twoActionCode";
                        m.DefectCode = "twoDefectCode";

                        acs.Add(new SelectListItem() { Value = "twoActionCode", Text = "twoActionCode" });
                        acs.Add(new SelectListItem() { Value = "twoActionCode2", Text = "twoActionCode2" });
                        acs.Add(new SelectListItem() { Value = "twoActionCode3", Text = "twoActionCode3" });

                        dcs.Add(new SelectListItem() { Value = "twoDefectCode", Text = "twoDefectCode" });
                        dcs.Add(new SelectListItem() { Value = "twoDefectCode2", Text = "twoDefectCode2" });
                        dcs.Add(new SelectListItem() { Value = "twoDefectCode3", Text = "twoDefectCode3" });

                        break;
                    }
                default:
                    {
                        m.ActionCode = $"{id}ActionCode";
                        m.DefectCode = $"{id}DefectCode";

                        acs.Add(new SelectListItem() { Value = $"{id}ActionCode", Text = $"{id}ActionCode" });
                        acs.Add(new SelectListItem() { Value = $"{id}ActionCode2", Text = $"{id}ActionCode2" });
                        acs.Add(new SelectListItem() { Value = $"{id}ActionCode3", Text = $"{id}ActionCode3" });

                        dcs.Add(new SelectListItem() { Value = $"{id}DefectCode", Text = $"{id}DefectCode" });
                        dcs.Add(new SelectListItem() { Value = $"{id}DefectCode2", Text = $"{id}DefectCode2" });
                        dcs.Add(new SelectListItem() { Value = $"{id}DefectCode3", Text = $"{id}DefectCode3" });

                        break;
                    }
            }

            m.ActionCodes = acs;
            m.DefectCodes = dcs;

            return PartialView(m);
        }

        public ActionResult MoreThanOnePartialViews(string id)
        {
            Models.FATO.FATOModel m = new Models.FATO.FATOModel();
            m.inputModel = new Models.FATO.FATOInputModel() { SerialNumber = id };
            m.codeModel = new Models.FATO.FATOCodeModel();
            m.detailsModel = new Models.FATO.FATODetailsModel();

            m.gridModel = new Models.FATO.FATOGrid();
            m.gridModel.Rows = new List<Models.FATO.FATORow>() { new Models.FATO.FATORow() { One = "5", Two = "2", Three = "3", Four = "4" },
                new Models.FATO.FATORow() { One = "6", Two = "22", Three = "23", Four = "24" },
                new Models.FATO.FATORow() { One = "7", Two = "32", Three = "33", Four = "34" },
                new Models.FATO.FATORow() { One = "8", Two = "42", Three = "43", Four = "44" }
            };

            switch (id.First())
            {
                case '1':
                    {
                        m.detailsModel.BCN = "oneBCN";
                        m.detailsModel.ClientName = "oneClient";
                        m.detailsModel.CompanyName = "oneCompany";
                        m.detailsModel.PartNumber = "onePart";

                        break;
                    }
                case '2':
                    {
                        m.detailsModel.BCN = "twoBCN";
                        m.detailsModel.ClientName = "twoClient";
                        m.detailsModel.CompanyName = "twoCompany";
                        m.detailsModel.PartNumber = "twoPart";

                        break;
                    }
                default:
                    {
                        m.detailsModel.BCN = $"{id}BCN";
                        m.detailsModel.ClientName = $"{id}Client";
                        m.detailsModel.CompanyName = $"{id}Company";
                        m.detailsModel.PartNumber = $"{id}Part";

                        break;
                    }
            }

            var acs = new List<SelectListItem>();
            var dcs = new List<SelectListItem>();


            switch (id.First())
            {
                case '1':
                    {
                        m.codeModel.ActionCode = "oneActionCode";
                        m.codeModel.DefectCode = "oneDefectCode";

                        acs.Add(new SelectListItem() { Value = "oneActionCode", Text = "oneActionCode" });
                        acs.Add(new SelectListItem() { Value = "oneActionCode2", Text = "oneActionCode2" });
                        acs.Add(new SelectListItem() { Value = "oneActionCode3", Text = "oneActionCode3" });

                        dcs.Add(new SelectListItem() { Value = "oneDefectCode", Text = "oneDefectCode" });
                        dcs.Add(new SelectListItem() { Value = "oneDefectCode2", Text = "oneDefectCode2" });
                        dcs.Add(new SelectListItem() { Value = "oneDefectCode3", Text = "oneDefectCode3" });

                        break;
                    }
                case '2':
                    {
                        m.codeModel.ActionCode = "twoActionCode";
                        m.codeModel.DefectCode = "twoDefectCode";

                        acs.Add(new SelectListItem() { Value = "twoActionCode", Text = "twoActionCode" });
                        acs.Add(new SelectListItem() { Value = "twoActionCode2", Text = "twoActionCode2" });
                        acs.Add(new SelectListItem() { Value = "twoActionCode3", Text = "twoActionCode3" });

                        dcs.Add(new SelectListItem() { Value = "twoDefectCode", Text = "twoDefectCode" });
                        dcs.Add(new SelectListItem() { Value = "twoDefectCode2", Text = "twoDefectCode2" });
                        dcs.Add(new SelectListItem() { Value = "twoDefectCode3", Text = "twoDefectCode3" });

                        break;
                    }
                default:
                    {
                        m.codeModel.ActionCode = $"{id}ActionCode";
                        m.codeModel.DefectCode = $"{id}DefectCode";

                        acs.Add(new SelectListItem() { Value = $"{id}ActionCode", Text = $"{id}ActionCode" });
                        acs.Add(new SelectListItem() { Value = $"{id}ActionCode2", Text = $"{id}ActionCode2" });
                        acs.Add(new SelectListItem() { Value = $"{id}ActionCode3", Text = $"{id}ActionCode3" });

                        dcs.Add(new SelectListItem() { Value = $"{id}DefectCode", Text = $"{id}DefectCode" });
                        dcs.Add(new SelectListItem() { Value = $"{id}DefectCode2", Text = $"{id}DefectCode2" });
                        dcs.Add(new SelectListItem() { Value = $"{id}DefectCode3", Text = $"{id}DefectCode3" });

                        break;
                    }
            }

            m.codeModel.ActionCodes = acs;
            m.codeModel.DefectCodes = dcs;

            return PartialView(m);
        }
    }
}
