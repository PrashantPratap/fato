﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FATOWeb.Models.FATO
{
    public class FATOModel
    {
        public FATOInputModel inputModel { get; set; }
        public FATODetailsModel detailsModel { get; set; }
        public FATOCodeModel codeModel { get; set; }
        public FATOGrid gridModel { get; set; }

        public bool showpVew = true;

        //public FATO1 {get;set;}
    }

    public class FATOInputModel
    {
        public string GeoLocation { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> GeoLocations { get; set; }
        public string WorkCenter { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> WorkCenters { get; set; }
        public int ScanType { get; set; }
        public string SerialNumber { get; set; }
    }

    public class FATODetailsModel
    {
        public string PartNumber { get; set; }
        public string BCN { get; set; }
        public string ClientName { get; set; }
        public string CompanyName { get; set; }
    }

    public class FATOCodeModel
    {
        public string DefectCode { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> DefectCodes { get; set; }
        public string ActionCode { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> ActionCodes { get; set; }
    }

    public class FATOTreeModel
    {
        public string TCSPass { get; set; }
    }

    public class FATOResults
    {
        public string Results { get; set; }
        public string Notes { get; set; }
    }

    public class FATORow
    {
        public string One { get; set; }
        public string Two { get; set; }
        public string Three { get; set; }
        public string Four { get; set; }
    }

    public class FATOGrid
    {
        public List<FATORow> Rows { get; set; }
    }
}