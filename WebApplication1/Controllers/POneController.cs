﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class POneController : Controller
    {
        // GET: POne
        public ActionResult Index()
        {
            return View();
        }

        // GET: POne/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: POne/Create
        public ActionResult Create()
        {
            return View(new Pone() { FirstName="pratap" });
        }

        // POST: POne/Create
        [HttpPost]
        public ActionResult Create(Pone m)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: POne/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: POne/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: POne/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: POne/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
