﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OneMVC.DB;

namespace OneMVC.Controllers
{
    public class AzInsightsController : Controller
    {
        private SniperDBContext db = new SniperDBContext();

        // GET: AzInsights
        public ActionResult Index()
        {
            return View(db.AzInsightsUpdates.ToList());
        }

        // GET: AzInsights/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AzInsightsUpdate azInsightsUpdate = db.AzInsightsUpdates.Find(id);
            if (azInsightsUpdate == null)
            {
                return HttpNotFound();
            }
            return View(azInsightsUpdate);
        }

        // GET: AzInsights/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AzInsights/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,LastOutlookCasesDatetime,LastMSSolveUpdateDatetime,LastSSRSGetDatetime")] AzInsightsUpdate azInsightsUpdate)
        {
            if (ModelState.IsValid)
            {
                db.AzInsightsUpdates.Add(azInsightsUpdate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(azInsightsUpdate);
        }

        // GET: AzInsights/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AzInsightsUpdate azInsightsUpdate = db.AzInsightsUpdates.Find(id);
            if (azInsightsUpdate == null)
            {
                return HttpNotFound();
            }
            return View(azInsightsUpdate);
        }

        // POST: AzInsights/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,LastOutlookCasesDatetime,LastMSSolveUpdateDatetime,LastSSRSGetDatetime")] AzInsightsUpdate azInsightsUpdate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(azInsightsUpdate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(azInsightsUpdate);
        }

        // GET: AzInsights/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AzInsightsUpdate azInsightsUpdate = db.AzInsightsUpdates.Find(id);
            if (azInsightsUpdate == null)
            {
                return HttpNotFound();
            }
            return View(azInsightsUpdate);
        }

        // POST: AzInsights/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AzInsightsUpdate azInsightsUpdate = db.AzInsightsUpdates.Find(id);
            db.AzInsightsUpdates.Remove(azInsightsUpdate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
