﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About(int id)
        {
            ViewBag.Message = $"id value is {id}";
            return View();
        }

        public ActionResult Contact(int id, string str)
        {
            ViewBag.Message = $"id value is {id} and str value is {str}";
            return View();
        }
    }
}