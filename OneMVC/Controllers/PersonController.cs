﻿using OneMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneMVC.Controllers
{
    public class PersonController : Controller
    {
        // GET: Person
        public ActionResult Index()
        {
            return View(persons);
        }

        // GET: Person/Details/5
        public ActionResult Details(int id)
        {
            Person m = null;
            var ps = persons.Where(p => p.ID == id);
            if (ps == null || ps.Count() == 0)
            {
                m = new Person();
            }
            else
            {
                m = ps.First();
            }
            return View(m);
        }

        // GET: Person/Create
        public ActionResult Create()
        {
            return View(new Person());
        }

        // POST: Person/Create
        [HttpPost]
        public ActionResult Create(Person m)
        {
            persons.Add(m);
            return RedirectToAction("Index");
        }

        // GET: Person/Edit/5
        public ActionResult Edit(int id)
        {
            Person m = null; 
            var ps = persons.Where(p => p.ID == id);
            if (ps == null || ps.Count() == 0)
            {
                m = new Person();
            }
            else
            {
                m = ps.First();
            }
            return View(m);
        }

        // POST: Person/Edit/5
        [HttpPost]
        public ActionResult Edit(Person inputModel)
        {
            try
            {
                Person m = null;
                var ps = persons.Where(p => p.ID == inputModel.ID);
                if (ps != null )
                {
                    m = ps.First();
                    m.Phone = inputModel.Phone;
                    m.FirstName = inputModel.FirstName;
                    m.LastName = inputModel.LastName;
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Person/Delete/5
        public ActionResult Delete(int id)
        {
            Person m = null;
            var ps = persons.Where(p => p.ID == id);
            if (ps != null )
            {
                m = ps.First();
            }
            else
            {
                m = new Person();
            }
            return View(m);
        }

        // POST: Person/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Person m = null;
                var ps = persons.Where(p => p.ID == id);
                if (ps != null)
                {
                    m = ps.First();
                    persons.Remove(m);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        static List<Person> persons = new List<Person>()
       {
            new Person { ID = 1, FirstName = "Tomato Soup", LastName = "Groceries",  Phone = "1" },
            new Person { ID = 2, FirstName = "Yo-yo", LastName = "Toys", Phone = "3.75M" },
            new Person { ID = 3, FirstName = "Hammer", LastName = "Hardware", Phone = "16.99M" }
       };
    }
}
