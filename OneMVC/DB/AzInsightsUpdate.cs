namespace OneMVC.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AzInsightsUpdate
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime LastOutlookCasesDatetime { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime LastMSSolveUpdateDatetime { get; set; }

        [Key]
        [Column(Order = 3)]
        public DateTime LastSSRSGetDatetime { get; set; }
    }
}
