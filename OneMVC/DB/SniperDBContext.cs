namespace OneMVC.DB
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SniperDBContext : DbContext
    {
        public SniperDBContext()
            : base("name=SniperDBContext")
        {
        }

        public virtual DbSet<AzInsightsUpdate> AzInsightsUpdates { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
